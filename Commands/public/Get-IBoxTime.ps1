function Get-IBoxTime {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][Connection]$Connection
    )

    $CmdLetParams = Get-IBoxWebRequestParameter -Connection $Connection
    $CmdLetParams.add("Uri", ($Connection.Address.AbsoluteUri + "sysbus/Time:getTime"))
    
    $CmdLetParams.add("Body", "{`"parameters`":{}}")
    $CmdLetParams.add("Method", "Post")

    $Time = Invoke-Restmethod @CmdLetParams

    $CmdLetParams["Uri"] = ($Connection.Address.AbsoluteUri + "sysbus/Time:getLocalTimeZoneName")
    $Timezone = Invoke-Restmethod @CmdLetParams

    return [PSCustomObject]@{
        Time = $Time.data.time
        Timezone = $Timezone.data.timezone
    }
}