function Get-IBoxDevice {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][uri]$Address
    )

    $Uri = $Address.AbsoluteUri + "sysbus/DeviceInfo"
    $Result = Invoke-Restmethod -uri $Uri

    $ReturnValue = @{}
    foreach ($Parameter in $Result.parameters) {
        if (-not $ReturnValue.ContainsKey($Parameter.name)) {
            $ReturnValue.Add($Parameter.name, $Parameter.Value)
        }
    }

    return [PSCustomObject]$ReturnValue
}