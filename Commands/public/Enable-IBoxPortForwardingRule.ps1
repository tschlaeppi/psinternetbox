function Enable-IBoxPortForwardingRule {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][Connection]$Connection,
        [Parameter(Mandatory=$true)][PortForwardingRule]$InputObject
    )

    $InputObject.Enable = $true
    return Set-IBoxPortForwardingRule -Connection $Connection -InputObject $InputObject
}