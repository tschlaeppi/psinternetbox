function New-IBoxPortForwardingRule {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][Connection]$Connection,
        [Parameter(Mandatory=$true)][string]$Id,
        [Parameter(Mandatory=$false)][string]$Origin = "webui",
        [Parameter(Mandatory=$true)][string]$Description,
        [Parameter(Mandatory=$true)][EnabledStatus]$Status,
        [Parameter(Mandatory=$false)][string]$SourceInterface = "data",
        [Parameter(Mandatory=$true)][string]$Protocol,
        [Parameter(Mandatory=$true)][string]$ExternalPort,
        [Parameter(Mandatory=$true)][string]$InternalPort,
        [Parameter(Mandatory=$false)][string]$SourcePrefix,
        [Parameter(Mandatory=$true)][string]$DestinationIpAddress,
        [Parameter(Mandatory=$false)][boolean]$Persistent = $true
    )

    $ExistingRule = Get-IBoxPortForwardingRule -Connection $Connection -Id $Id -Origin $Origin -ErrorAction SilentlyContinue
    if ($null -ne $ExistingRule) {
        throw "Portforwarding Rule already exists"
    }

    $NewRule = [PortForwardingRule]::new()
    $NewRule.ConvertFromGenericObjectResponse($PSBoundParameters)
    $NewRule.Origin = $Origin

    return Set-IBoxPortForwardingRule -Connection $Connection -InputObject $NewRule
}