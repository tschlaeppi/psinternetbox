function Set-IBoxPortForwardingRule {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][Connection]$Connection,
        [Parameter(Mandatory=$true)][PortForwardingRule]$InputObject
    )

    $Parameters = @{"parameters"=$InputObject.GetUpdateRequestObject()}

    $CmdLetParams = Get-IBoxWebRequestParameter -Connection $Connection
    $CmdLetParams.add("Uri", ($Connection.Address.AbsoluteUri + "sysbus/Firewall:setPortForwarding"))
    $CmdLetParams.add("Body", ($Parameters | ConvertTo-Json -Compress))
    $CmdLetParams.add("Method", "Post")

    $PortForwardingResult = Invoke-Restmethod @CmdLetParams
    $ResponseObject = [PortForwardingRule]::new();
    $ResponseObject.ConvertFromGenericObjectResponse($PortForwardingResult.data.rule)
    return $ResponseObject
}