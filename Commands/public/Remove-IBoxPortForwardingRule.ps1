function Remove-IBoxPortForwardingRule {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][Connection]$Connection,
        [Parameter(Mandatory=$true)][PortForwardingRule]$InputObject
    )

    $Parameters = @{"parameters"=@{
        "id" = $InputObject.Id
        "origin" = $InputObject.Origin
        "destinationIPAddress" = $InputObject.DestinationIpAddress.IPAddressToString
    }}

    $CmdLetParams = Get-IBoxWebRequestParameter -Connection $Connection
    $CmdLetParams.add("Uri", ($Connection.Address.AbsoluteUri + "sysbus/Firewall:deletePortForwarding"))
    $CmdLetParams.add("Body", ($Parameters | ConvertTo-Json -Compress))
    $CmdLetParams.add("Method", "Post")

    $PortForwardingResult = Invoke-Restmethod @CmdLetParams
    Write-Verbose "Result: $($PortForwardingResult | ConvertTo-Json)"

    return
}