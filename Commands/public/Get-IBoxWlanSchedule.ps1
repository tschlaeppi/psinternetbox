function Get-IBoxWlanSchedule {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][Connection]$Connection
    )

    $CmdLetParams = Get-IBoxWebRequestParameter -Connection $Connection
    $CmdLetParams.add("Uri", ($Connection.Address.AbsoluteUri + "sysbus/Scheduler:getCompleteSchedules"))
    $CmdLetParams.add("Body", "{`"parameters`":{`"type`":`"WLAN`"}}")
    $CmdLetParams.add("Method", "Post")

    $Result = Invoke-Restmethod @CmdLetParams

    return $Result.data.scheduleInfo

}