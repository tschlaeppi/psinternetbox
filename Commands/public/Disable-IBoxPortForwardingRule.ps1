function Disable-IBoxPortForwardingRule {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][Connection]$Connection,
        [Parameter(Mandatory=$true)][PortForwardingRule]$InputObject
    )

    $InputObject.Enable = $false
    return Set-IBoxPortForwardingRule -Connection $Connection -InputObject $InputObject
}