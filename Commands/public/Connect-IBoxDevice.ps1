function Connect-IBoxDevice {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][uri]$Address,
        [Parameter(Mandatory=$true)][pscredential]$Credential
    )

    <# TODO: Remove Pairing if not necessary
    $Uri = $Address.AbsoluteUri.TrimEnd("/") + ":15700/pair?username=$($Credential.UserName)&password=$($Credential.GetNetworkCredential().Password)"   
    $Result = Invoke-Restmethod -uri $Uri -SessionVariable "WebSession" -Headers @{Cookie="SERVICE=VERIFY"}
    #>
    
    $Context = New-IBoxContext -Address $Address.AbsoluteUri -Credential $Credential -Context "sah.Device.Information"
    
    return [Connection]::new($Address, $Context)
}