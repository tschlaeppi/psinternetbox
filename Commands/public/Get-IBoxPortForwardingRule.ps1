function Get-IBoxPortForwardingRule {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true, ParameterSetName="GetAll")]
        [Parameter(Mandatory=$true, ParameterSetName="GetSingle")][Connection]$Connection,
        [Parameter(Mandatory=$true, ParameterSetName="GetSingle")][string]$Id,
        [Parameter(Mandatory=$false, ParameterSetName="GetSingle")][string]$Origin = "webui"
    )

    $Parameters = @{"parameters"=@{}}
    if ($PSCmdlet.ParameterSetName -eq "GetSingle") {
        $Parameters.parameters.Add("id", $Id)
        $Parameters.parameters.Add("origin", $Origin)
    }

    $CmdLetParams = Get-IBoxWebRequestParameter -Connection $Connection
    $CmdLetParams.add("Uri", ($Connection.Address.AbsoluteUri + "sysbus/Firewall:getPortForwarding"))
    $CmdLetParams.add("Body", ($Parameters | ConvertTo-Json -Compress))
    $CmdLetParams.add("Method", "Post")

    $PortForwardingResult = Invoke-Restmethod @CmdLetParams

    $ReturnObject = @()

    foreach ($Property in ($PortForwardingResult.status | Get-Member -MemberType NoteProperty)) {

        $RawResponseObject = $PortForwardingResult.status | Select-Object -ExpandProperty $Property.Name
        $ResponseObject = [PortForwardingRule]::new();
        $ResponseObject.ConvertFromGenericObjectResponse($RawResponseObject)
        $ReturnObject += $ResponseObject
    }

    return $ReturnObject
}