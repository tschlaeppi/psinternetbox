function New-IBoxContext {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][uri]$Address,
        [Parameter(Mandatory=$true)][string]$Context,
        [Parameter(Mandatory=$true)][pscredential]$Credential
    )   

    $CmdLetParams = @{
        "Uri" = $Address.AbsoluteUri + "ws"
        "Headers" = @{
            "Authorization" = "X-Sah-Login"
        }
        "UserAgent" = "X-Sah-Login"
        "ContentType" = "application/x-sah-ws-4-call+json"
        "Method" = "Post"
        "Body" = "{`"method`":`"createContext`",`"service`":`"$($Context)`",`"parameters`":{`"password`":`"$($Credential.GetNetworkCredential().Password)`",`"applicationName`":`"webui`",`"username`":`"$($Credential.username)`"}}"
    }

    $Result = Invoke-Restmethod @CmdLetParams -SessionVariable "IBoxContextSession"

    return [Context]::new($Result.data.contextId, $Result.data.username, $Result.data.groups, $IBoxContextSession)
}