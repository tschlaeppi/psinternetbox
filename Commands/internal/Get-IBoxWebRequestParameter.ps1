function Get-IBoxWebRequestParameter {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][Connection]$Connection
    )

    $CookieHeader = Get-IBoxCookieHeader -Connection $Connection

    return @{
        "Headers" = @{
            "Authorization" = " X-Sah $($Connection.Context.ContextId)"
            "X-Context" = $Connection.Context.ContextId
            "Cookie"=$CookieHeader
        }
        "UserAgent" = "PSInternetBox"
        "ContentType" = "application/x-sah-ws-4-call+json"
    }
}