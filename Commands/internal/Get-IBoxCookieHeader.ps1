function Get-IBoxCookieHeader {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][Connection]$Connection
    )

    $ContextCookie = $Connection.Context.Session.Cookies.GetAllCookies()
    

    return "swc/deviceID=$(($ContextCookie.Name -split "/")[0]); $(($ContextCookie.Name -split "/")[0])/sessid=$($ContextCookie.Value); $(($ContextCookie.Name -split "/")[0])/weakReference=$($Connection.Context.ContextId)"
}