#region Connection

class Connection {
    [uri]$Address
    [Context]$Context

    Connection([uri]$Address, [Context]$Context) {
        $this.Address = $Address
        $this.Context = $Context
    }
}

class Context {
    [string]$ContextId
    [string]$Username
    [string[]]$Groups
    [Microsoft.PowerShell.Commands.WebRequestSession]$Session

    Context([string]$ContextId,[string]$Username,[string]$Groups, [Microsoft.PowerShell.Commands.WebRequestSession]$Session) {
        $this.ContextId = $ContextId
        $this.Username = $Username
        $this.Groups = $Groups.Split(",")
        $this.Session = $Session
    }
}
#endregion

enum EnabledStatus {
    Enabled = 0
    Disabled = 1
}

enum PortForwardingProtocol {
    TCP = 6
    UDP = 17
}

#region Entities
class PortForwardingRule {
    [string]$Id
    [string]$Origin
    [string]$Description
    [EnabledStatus]$Status
    [string]$SourceInterface
    [PortForwardingProtocol]$Protocol
    [int]$ExternalPort
    [int]$InternalPort
    [string]$SourcePrefix 
    [ipaddress]$DestinationIPAddress
    [string]$DestinationMACAddress
    [int]$LeaseDuration
    [bool]$HairpinNAT
    [bool]$SymmetricSNAT
    [bool]$UPnPV1Compat
    [bool]$Enable

    ConvertFromGenericObjectResponse($InputObject) {
        $this.Id = $InputObject.Id
        $this.Origin = $InputObject.Origin
        $this.Description = $InputObject.Description
        $this.Status = $InputObject.Status
        $this.SourceInterface = $InputObject.SourceInterface
        $this.Protocol = $InputObject.Protocol
        $this.ExternalPort = $InputObject.ExternalPort
        $this.InternalPort = $InputObject.InternalPort
        $this.SourcePrefix  = $InputObject.SourcePrefix
        $this.DestinationIPAddress = $InputObject.DestinationIPAddress
        $this.DestinationMACAddress = $InputObject.DestinationMACAddress
        $this.LeaseDuration = $InputObject.LeaseDuration
        $this.HairpinNAT = $InputObject.HairpinNAT
        $this.SymmetricSNAT = $InputObject.SymmetricSNAT
        $this.UPnPV1Compat = $InputObject.UPnPV1Compat
        $this.Enable = $InputObject.Enable
    }
    
    [hashtable]GetUpdateRequestObject() {
        return @{
            "id" = $this.Id
            "origin" = $this.Origin
            "description" = $this.Description
            "enable" = $this.Enable
            "sourceInterface" = $this.SourceInterface 
            "protocol" =  $this.Protocol.value__
            "externalPort" = $this.ExternalPort
            "internalPort" = $this.InternalPort
            "sourcePrefix" = $this.SourcePrefix
            "destinationIPAddress" = $this.DestinationIpAddress.IPAddressToString
            "persistent" = $this.Persistent
        }
    }
}
#endregion[]