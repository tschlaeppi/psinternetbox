$ErrorActionPreference = "Stop"


$ModuleRootPath = Split-Path -Path $MyInvocation.MyCommand.Path -Parent

# Load types
$TypesPath = Join-Path -Path $ModuleRootPath -ChildPath "Types\Types.ps1"
if (Test-Path -Path $TypesPath) {
    . $TypesPath
}

# load commandlets
$CommandsRootPath = Join-Path -Path $ModuleRootPath -ChildPath "Commands"
$MemberFunctions = Get-ChildItem -Path $CommandsRootPath -Recurse -Include "*.ps1"

foreach ($MemberFunction in $MemberFunctions) {
    Write-Verbose "Loading function $($MemberFunction.FullName)"
    . $MemberFunction.FullName
}