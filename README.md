# PSInternetBox Powershell Module
The PSInternetBox Powershell Module interacts with the API of the Swisscom Internet Box

## Usage
```
# Get InternetBox
Get-IBoxDevice -Address "http://192.168.1.1"


# Get Credentials for accessing the Internet Box and establish connection
$Credentials = Get-Credential
$Connection = Connect-IBoxDevice -Address "http://192.168.1.1" -Credential $Creds

# Get WLAN Schedules
Get-IBoxWlanSchedule -Connection $Connection

# Get Time
Get-IboxTime -Connection $Connection

# Get Port forwading
Get-IBoxPortForwardingRule -Connection $Connection

# Add a new port forwarding rule
$NewPortForwardingRule = New-IBoxPortForwardingRule -Connection $Connection -Id "MyWebSite" -Description "My Website exposed to internet" -Status "Disabled" -Protocol "TCP" -ExternalPort "667" -InternalPort "80" -DestinationIpAddress "192.168.1.104" -Persistent $true

# Enable or disable a port forwarding rule
Disable-IBoxPortForwardingRule -Connection $Connection -InputObject $NewPortForwardingRule
Enable-IBoxPortForwardingRule -Connection $Connection -InputObject $NewPortForwardingRule

# remove a port forwarding rule
Remove-IBoxPortForwardingRule -Connection $Connection -InputObject $NewPortForwardingRule




```